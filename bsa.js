import Chat from './client/src/components/Chat/Chat';
import rootReducer from './client/src/reducers/rootReducer';

export default {
	Chat,
	rootReducer,
};
