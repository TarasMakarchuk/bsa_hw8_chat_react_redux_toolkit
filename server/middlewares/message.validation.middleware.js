const { message } = require('../models/message');
const { validateData, validateFields } = require('../middlewares/validation.middleware');

const validateMessage = (req, res, next) => {
    const { userId, user, text } = req.body;

    validateData(req, res);
    validateFields(req, res, message);

    if (!userId || userId === '') {
        res.status(400).send({
            error: true,
            message: `UserId is required`,
        });
        return false;
    }

    if (!user || userId === '') {
        res.status(400).send({
            error: true,
            message: `User is required`,
        });
        return false;
    }

     if (!text || userId === '') {
        res.status(400).send({
            error: true,
            message: `Text is required`,
        });
         return false;
    }

     return true;
};

const createMessageValid = (req, res, next) => {

    const valid = validateMessage(req, res, next);
    next();
    return valid;
}

const updateMessageValid = (req, res, next) => {

    const valid = validateMessage(req, res, next);
    next();
    return valid;
}

exports.createMessageValid = createMessageValid;
exports.updateMessageValid = updateMessageValid;