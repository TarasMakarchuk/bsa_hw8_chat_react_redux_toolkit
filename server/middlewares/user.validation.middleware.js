const { user } = require('../models/user');
const { validateData, validateFields } = require('../middlewares/validation.middleware');
const userService = require('../services/userService');

const emailPattern = new RegExp(/([a-zA-Z0-9]+)([.{1}])?([a-zA-Z0-9]+)@gmail([.])com/g);
const MIN_FIRST_NAME_LENGTH = 2;
const MIN_PASSWORD_LENGTH = 3;

const validateCreateUser = ((req, res, next) => {
const { userName, email, password, role } = req.body;

    validateData(req, res);
    validateFields(req, res, user);

    if (!userName) {
        res.status(400).send({
            error: true,
            message: `UserName is required`,
        });
        return false;
    } else if (userName.length < MIN_FIRST_NAME_LENGTH) {
        res.status(400).send({
            error: true,
            message: `UserName should be less than ${MIN_FIRST_NAME_LENGTH} symbols`,
        });
        return false;
    }

    if(userService.search(req.body) !== null){
        res.status(400).send({
            error: true,
            message: `User already exists`,
        });
        return false;
    }

    if (!email) {
        res.status(400).send({
            error: true,
            message: `Email is required`,
        });
        return false;
    } else if (!email.match(emailPattern)) {
        res.status(400).send({
            error: true,
            message: `Email ${email} should be registered on gmail.com`,
        });
        return false;
    }

    if (!password) {
        res.status(400).send({
            error: true,
            message: `Password is required`,
        });
        return false;
    } else if (password.length < MIN_PASSWORD_LENGTH) {
        res.status(400).send({
            error: true,
            message: `Password length should be more that ${MIN_PASSWORD_LENGTH} symbols`,
        });
        return false;
    }
    return true;
});

const createUserValid = (req, res, next) => {

    const valid = validateCreateUser(req, res, next);
    next();
    return valid;
}

const updateUserValid = (req, res, next) => {

    const valid = validateCreateUser(req, res, next);
    next();
    return valid;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;