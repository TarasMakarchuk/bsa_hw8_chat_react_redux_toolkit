const validateData = (req, res) =>{
	if (!req && !req.body) {
		res.status(400).send({
			error: true,
			message: `Data is not received`,
		});
		return false;
	}
};

const validateFields = (req, res, role) =>{
	const requestFields = Object.keys(req.body);
	const userFields = Object.keys(role).filter(item => item !== 'id');
	const checkFields = (array, target) => target.every(field => array.includes(field));
	const areFieldsValid = checkFields(userFields, requestFields);

	if (!areFieldsValid) {
		res.status(400).send({
			error: true,
			message: `Fields names are incorrect`,
		});
		return false;
	}
};

module.exports = {
	validateData,
	validateFields
};
