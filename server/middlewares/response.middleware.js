const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   //
   //  if (result) {
   //      res.status(200).send({"Status": "OK"});
   //      next();
   //  } else if (!result) {
   //      res.status(400).send({
   //            error: true,
   //            message: 'Bad Request',
   //        });
   //  } else {
   //      res.status(404).send({
   //          error: true,
   //          message: 'Not found',
   //      });
   //  }
   //  next();


  const { statusCode, message } = res.err || {};
  if (message) {
    res.statusCode = statusCode;
    res.json({ error: true, message });
  } else {
    res.json(res.locals);
  }
  next();
}

exports.responseMiddleware = responseMiddleware;