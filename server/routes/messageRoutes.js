const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createMessageValid, updateMessageValid } = require('../middlewares/message.validation.middleware');

const router = Router();


router.get('/', function (req, res, next) {
	res.send(MessageService.getAllMessagesData());
});

router.get('/:id', function (req, res, next) {
	const { id } = req.params;
	const result = MessageService.getMessageById(id);
	if (result) {
		res.send(`Data ${JSON.stringify(result)}`);
	} else {
		res.status(400).send(`Some error`);
	}
});

router.post('/', createMessageValid, function (req, res, next) {
	if (createMessageValid(req, res, next)) {
		const result = MessageService.saveMessageData(req.body);
		if (result) {
			res.send(`Data saved ${JSON.stringify(result)}`);
		} else {
			res.status(400).send(`Some error`);
		}
	}
});

router.put('/:id', updateMessageValid, function (req, res, next) {
	const { id } = req.params;
	if (updateMessageValid(req, res, next)) {
		const result = MessageService.updateMessageData(id, req.body);
		if (result) {
			res.send(`Data updated ${JSON.stringify(result)}`);
		} else {
			res.status(400).send(`Some error`);
		}
	}
});

router.delete('/:id', function (req, res, next) {
	const { id } = req.params;
	const result = MessageService.deleteMessage(id);
	if (result) {
		res.send(`Deleted ${JSON.stringify(result)}`);
	} else {
		res.status(400).send(`Some error`);
	}
});

module.exports = router;