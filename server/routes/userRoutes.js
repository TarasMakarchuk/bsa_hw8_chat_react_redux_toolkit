const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', function (req, res, next) {
	res.json(UserService.getAllUsersData());
});

router.get('/:id', function (req, res, next) {
	const { id } = req.params;
	const result = UserService.getUserById(id);
	if (result) {
		res.json(result);
	} else {
		res.status(400).send(`Some error`);
	}
});

router.post('/', createUserValid, function (req, res, next) {
	if (createUserValid(req, res, next)) {
		const result = UserService.saveUserData(req.body);
		if (result) {
			res.json(result);
		} else {
			res.status(400).send(`Some error`);
		}
	}
});

router.put('/:id', updateUserValid, function (req, res, next) {
	const { id } = req.params;
	if (updateUserValid(req, res, next)) {
		const result = UserService.updateUserData(id, req.body);
		if (result) {
			res.json(result);
		} else {
			res.status(400).send(`Some error`);
		}
	}
});

router.delete('/:id', function (req, res, next) {
	const { id } = req.params;
	const result = UserService.deleteUser(id);
	if (result) {
		res.json(result);
	} else {
		res.status(400).send(`Some error`);
	}
});

module.exports = router;