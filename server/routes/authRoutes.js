const {Router} = require('express');
const {login} = require('../services/authService');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

router.post('/', (req, res, next) => {
	try {
		const result = login(req.body);
		if (result !== null) {
			return res.status(200).send(result);
		} else {
			return res.send('Unauthorized')
		}
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
}, responseMiddleware);

module.exports = router;