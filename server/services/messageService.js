const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {

	getAllMessagesData = () => MessageRepository.getAll();

	getMessageById = id => {
		if (id) {
			return MessageRepository.getOneById(id);
		} else {
			return null;
		}
	};

	saveMessageData = data => {
		if (data) {
			return MessageRepository.create(data);
		} else {
			return null;
		}
	};

	updateMessageData = (id, data) => {
		if (id && data) {
			return MessageRepository.update(id, data);
		} else {
			return null;
		}
	};

	deleteMessage = id => {
		if (id) {
			return MessageRepository.delete(id);
		} else {
			return null;
		}
	};
}

module.exports = new MessageService();