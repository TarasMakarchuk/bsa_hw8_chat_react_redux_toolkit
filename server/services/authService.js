const UserService = require('./userService');

class AuthService {
	login(userData) {
		if (Object.keys(userData).length !== 0) {
			const user = UserService.search(userData);
			if (!user) {
				return null;
			}
			return user;
		}
		return null;
	}
}

module.exports = new AuthService();