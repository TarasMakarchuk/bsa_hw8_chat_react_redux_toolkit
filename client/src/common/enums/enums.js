const AppPath = {
	LOGIN: '/login',
	ADMIN: '/admin',
	USER_LIST: '/user-list',
	CHAT: '/chat',
	ANY: '*',
};

export { AppPath };