import React from 'react';
import './signOut.css';
import {Redirect} from "react-router";

const SignOut = () => {

	const [isClicked, setIsClicked] = React.useState(false);

	const onClickHandler = () => {
		setIsClicked(true);
	};

	if (isClicked) return <Redirect to={'/login'}/>

	return (
		<div className='sign-out'>
			<button
				onClick={() => onClickHandler()}
				className='sign-out-button'
			>Sign Out
			</button>
		</div>
	)
};

export default SignOut;