import React from 'react';
import './header.css';
import Preloader from "../Preloader/Preloader";
import Moment from 'react-moment';
import moment from 'moment';
import {useSelector} from "react-redux";

const Header = () => {

	const {preloader, messages} = useSelector(state => state.rootReducer.chatReducer);

	const copyMessages = [];
	if (messages) {
		copyMessages.push(...messages)
	}

	let lastDate = '';
	let participants = [];

	if (copyMessages !== null) {
		const moments = copyMessages.map(item => moment(item.createdAt));
		lastDate = moment.max(moments);
		copyMessages.forEach(item => participants.push(item.userId));
		participants = [...new Set(participants)];
	}

	return (
		<div className='header'>
			<div className='header-info-block'>

				<div className='header-title'>
					<span>My chat</span>
				</div>

				<div className='header-users-count'>
					<span>{participants.length} participants</span>
				</div>

				<div className='header-messages-count'>
					<div>
						{preloader ?
							<Preloader/> :
							copyMessages.length
						}
					</div>
					<div>
						<span>messages</span>
					</div>
				</div>
			</div>

			<div className='header-last-message-block'>
				<div className='header-last-message-date'>
					<span>Last message at</span>
					<div>
						{preloader ?
							<Preloader/> :
							<Moment
								format="DD.MM.YYYY HH:mm"
							>
								{lastDate}
							</Moment>
						}
					</div>
				</div>
			</div>

		</div>
	);
}

export default Header;