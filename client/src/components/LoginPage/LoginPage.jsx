import React, {useEffect} from 'react';
import './loginPage.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faKey} from '@fortawesome/free-solid-svg-icons'
import {faUser} from '@fortawesome/free-solid-svg-icons'
import logoImg from "../../assets/img/logo.png";
import {Redirect} from "react-router";
import axios from "axios";
import {AppPath} from "../../common/enums/enums";
import {useDispatch, useSelector} from "react-redux";
import * as userActions from "../UserList/userListActions";

const Login = () => {

	const API_URL_AUTH = `http://localhost:3050/api/auth`;

	const [login, setLogin] = React.useState('');
	const [password, sePassword] = React.useState('');
	const [authData, setAuthData] = React.useState('');

	const {role} = useSelector(state => state.rootReducer.usersReducer);
	const dispatch = useDispatch();

	const handleChangeLogin = event => {
		setLogin(event.target.value);
	};

	const handleChangePassword = event => {
		sePassword(event.target.value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();
		const authData = {
			email: login,
			password: password
		}
		const response = await axios.post(API_URL_AUTH, authData);
		setAuthData(response.data);
	};

	useEffect(() => {
		dispatch(userActions.setUserRole(authData.role));
	}, [authData, dispatch]);

	if (role === 'admin') return <Redirect to={AppPath.ADMIN}/>;
	if (role === 'user') return <Redirect to={AppPath.CHAT}/>;

	const unauthorizedError = authData === 'Unauthorized' &&
		<div className='unauthorized-error'>
			<span>Incorrect username or password</span>
		</div>;

	return (
		<div className='login-page'>

			<div className='logo-login-container'>
				<img src={logoImg} alt="chat logo"/>
			</div>

			<div className='login-container'>
				<form className="login-form">

					<div className='login-form-input-container'>
						<div>
								 <span className="input-group-text">
                   <FontAwesomeIcon icon={faUser}/>
                 </span>
						</div>
						<input onChange={handleChangeLogin} className='input-login' type="text" placeholder="login"/>

					</div>

					<div className='login-form-input-container'>
						<div>
								 <span className="input-group-text">
                   <FontAwesomeIcon icon={faKey}/>
                 </span>
						</div>
						<input
							onChange={handleChangePassword}
							className='input-password'
							type="password"
							placeholder="password"
						/>
					</div>

					{unauthorizedError}
					<button onClick={handleSubmit} type='submit' className='submit'>Sign In</button>

				</form>
				<p>Admin page - login: <b>admin</b>, password: <b>admin</b></p>
				<p>Chat page - login: <b>user</b>, password: <b>user</b></p>
			</div>
		</div>

	)
};

export default Login;