import React from 'react';
import './adminPage.css';
import {NavLink} from "react-router-dom";
import {Route, Switch} from "react-router";
import UserList from "../UserList/UserList";
import Chat from "../Chat/Chat";
import SignOut from "../SignOut/SignOut";

const AdminPage = () => {

	return (
		<div className='admin-page-container'>
			<div className='admin-page-sign-out-block'>
				<SignOut/>
			</div>
			<div className='admin-links'>
				<nav className='nav-bar'>
					<NavLink to='/user-list'>User list</NavLink>
					<NavLink to='/chat'>Chat</NavLink>
				</nav>

				<Switch>
					<Route exact path='/user-list' component={UserList}/>
					<Route exact path='/chat' component={Chat}/>
				</Switch>

			</div>
		</div>
	);
};

export default AdminPage;