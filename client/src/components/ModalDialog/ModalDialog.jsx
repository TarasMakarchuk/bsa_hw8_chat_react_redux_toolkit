import React, {useState} from 'react';
import * as actions from '../Chat/chatActions';
import store from "../../store/store";
import {useDispatch, useSelector} from "react-redux";

import './modalDialog.css';

const ModalDialog = () => {

	const { text, id } = useSelector(state => state.rootReducer.chatReducer.temporaryMessageData);
	const { editModal } = useSelector(() => store.getState().rootReducer.chatReducer);
	const [newText, setText] = useState(text);

	const dispatch = useDispatch();

	const onClose = () => {
		dispatch(actions.showModal());
	};

	const onSave = () => {
		dispatch(actions.updateMessage(id, newText));
	}

	const onChange = event => {
		const text = event.target.value;
		if (text !== '') {
			setText(text);
		}
	}

	const showModalStyle = editModal ? 'modal-shown ' : '';

	return (
		<div className={`edit-message-modal ${showModalStyle}`}>
			<div className='edit-message-container'>
				<h2>Edit message</h2>
				<textarea
					onChange={(event) => onChange(event)}
					className='edit-message-input' cols="100" rows="4"
					value={newText}
				/>
				<div className='edit-message-buttons-block'>
					<button className='edit-message-close' onClick={onClose}>Close</button>
					<button className='edit-message-button' onClick={onSave}>Save</button>
				</div>

			</div>
		</div>
	);
}

export default ModalDialog;