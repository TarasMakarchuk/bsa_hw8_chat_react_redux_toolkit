import React from 'react';
import './backButton.css';
import {Redirect} from "react-router";

const BackButton = () => {

	const [isClicked, setIsClicked] = React.useState(false);

	const onClickHandler = () => {
		setIsClicked(true);
	};

	if (isClicked) return <Redirect to={'/admin'}/>

	return (
		<div className='back'>
			<button
				onClick={() => onClickHandler()}
				className='back-button'
			>Back
			</button>
		</div>
	)
};

export default BackButton;