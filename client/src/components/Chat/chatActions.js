import {
	ADD_MESSAGE,
	UPDATE_MESSAGE,
	DELETE_MESSAGE,
	ADD_ALL_MESSAGES,
	SHOW_PRELOADER,
	SHOW_MODAL,
	ADD_TEMPORARY_MESSAGE_DATA
} from './chatActionTypes';
import {getNewId} from './chatService';

export const showPreloader = data => ({
	type: SHOW_PRELOADER,
	payload: {
		data
	}
});

export const showModal = () => ({
	type: SHOW_MODAL,
});

export const addAllMessages = data => ({
	type: ADD_ALL_MESSAGES,
	payload: {
		data
	}
});

export const addMessage = data => ({
	type: ADD_MESSAGE,
	payload: {
		id: getNewId(),
		data
	}
});

// export const addMessage = createAsyncThunk(ADD_MESSAGE, async (payload, {extra}) => ({
// 	message: await extra.chatService.create(payload)
// }))

export const addTemporaryMessageData = (id, data, index) => ({
	type: ADD_TEMPORARY_MESSAGE_DATA,
	payload: {
		id,
		data,
		index
	}
});

export const updateMessage = (id, data) => ({
	type: UPDATE_MESSAGE,
	payload: {
		id,
		data
	}
});

export const deleteMessage = id => ({
	type: DELETE_MESSAGE,
	payload: {
		id
	}
});
