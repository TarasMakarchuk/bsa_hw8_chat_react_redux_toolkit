import React, {useEffect} from 'react';
import Header from "../Header/Header";
import MessageList from "./MessageList/MessageList";
import './chat.css';
import logoImg from '../../assets/img/logo.png';
import MessageInput from "./MessageInput/MessageInput";
import Footer from "../Footer/Footer";
import PropTypes from "prop-types";
import {useDispatch, useSelector} from "react-redux";
import * as actions from './chatActions';
import ModalDialog from "../ModalDialog/ModalDialog";
import SignOut from "../SignOut/SignOut";
import BackButton from "../BackButton/BackButton";

const Chat = () => {

	const {editModal, messages} = useSelector(state => state.rootReducer.chatReducer);
	const {role} = useSelector(state => state.rootReducer.usersReducer);
	const dispatch = useDispatch();

	useEffect(() => {
		fetch('http://localhost:3050/api/messages')
			.then(response => response.json())
			.then(data => {
				if (data) {
					dispatch(actions.addAllMessages(data));
					setTimeout(() => {
						dispatch(actions.showPreloader(false))
					}, 400);
				}
			})
			.catch(error => console.error(error));
	}, [dispatch])

	const addMessage = message => {
		dispatch(actions.addMessage(message));
	}

	const onChangeMessage = (id, text) => {
		dispatch(actions.showModal());
		const index = messages.findIndex(item => item.id === id);
		if (id && text && index) {
			dispatch(actions.addTemporaryMessageData(id, text, index));
		}
	}

	const onDeleteMessage = id => {
		dispatch(actions.deleteMessage(id));
	}

	return (
		<div className='chat-container'>

			<div className='logo-container'>
				<img src={logoImg} alt="chat logo"/>
				{role === 'admin' && <BackButton/>}

				<SignOut/>
			</div>

			<div className='header-container'>
				<Header/>
			</div>

			<div className='content-container'>
				{editModal &&
				<ModalDialog onChangeMessage={() => onChangeMessage()}/>
				}
				<MessageList
					onDeleteMessage={(id) => onDeleteMessage(id)}
					onChangeMessage={(id, text) => onChangeMessage(id, text)}
				/>
				<MessageInput
					addMessage={(message) => addMessage(message)}
					handleSendOwnMessage={() => this.handleSendOwnMessage()}
					handleChangeInput={(event) => this.handleChangeInput(event)}
				/>
			</div>

			<div className='footer-container'>
				<Footer/>
			</div>

		</div>
	);
}

Chat.propTypes = {
	messages: PropTypes.arrayOf(
		PropTypes.shape({
			avatar: PropTypes.string,
			createdAt: PropTypes.string,
			editedAt: PropTypes.string,
			id: PropTypes.string,
			text: PropTypes.string,
			user: PropTypes.string,
			userId: PropTypes.string,
		})
	)
}

export default Chat;