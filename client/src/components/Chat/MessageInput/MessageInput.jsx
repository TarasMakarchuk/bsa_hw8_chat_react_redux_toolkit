import React, {useState} from 'react';
import './messageInput.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowCircleUp} from '@fortawesome/free-solid-svg-icons'
import moment from "moment";

const MessageInput = props => {

	const [state, setState] = useState({text: ''});

	const handleSendMessage = () => {
		if (state.text !== '') {
			props.addMessage(state);
			setState(prev => ({
				...prev,
				text: ''
			}));
		}
	}

	const handleChangeInput = (event) => {
		const message = event.target.value;
		setState(prev => ({
			...prev,
			id: '80f08600-1b8f-11e8-9629-' + Date.now(),
			userId: "9999-8600-1b8f-11e8-9629-c7eca82a-9999",
			avatar: '',
			user: "John",
			text: message,
			createdAt: moment.utc().format('YYYY-MM-DDTHH:mm:ss.SSS') + 'Z',
			editedAt: "",
		}));
	}

	return (
		<div className='message-input'>
				<textarea
					className='message-input-text' cols="100" rows="4"
					onChange={(event) => handleChangeInput(event)}
					value={state.text}
				/>
			<button
				className='message-input-button'
				onClick={() => handleSendMessage()}
			>
				Send <FontAwesomeIcon style={{color: 'red'}} className='icon-arrow-up' icon={faArrowCircleUp}/>
			</button>
		</div>
	);
}

export default MessageInput;