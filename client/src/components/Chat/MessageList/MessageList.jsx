import React from 'react';
import Message from "./Message/Message";
import OwnMessage from "./OwnMessage/OwnMessage";
import Preloader from "../../Preloader/Preloader";
import './messageList.css';
import {useSelector} from "react-redux";

const MessageList = props => {
	const {preloader, messages} = useSelector(state => state.rootReducer.chatReducer);

	const ownerUserId = '9999-8600-1b8f-11e8-9629-c7eca82a-9999';

	let copyMessages = [];
	if (messages) {
		messages.forEach((item) => {
			if (item.userId !== ownerUserId) {
				copyMessages.push(item);
			}
		})
	}

	let ownMessages = [];
	if (messages) {
		messages.forEach((item) => {
			if (item.userId === ownerUserId) {
				ownMessages.push(item);
			}
		})
	}

	copyMessages.sort((first, second) => new Date(first.createdAt) - new Date(second.createdAt));
	ownMessages.sort((first, second) => new Date(first.createdAt) - new Date(second.createdAt));

	return (
		<div className='message-list'>
			{preloader ?
				<Preloader/> :
				<div>
					{copyMessages.map(item => <Message
						key={Date.now() + Math.random()}
						message={item}
					/>)}
					<hr className='messages-divider'/>
					{ownMessages.map(item => <OwnMessage
							key={Date.now() + Math.random()}
							ownMessages={item}
							onDeleteMessage={(id) => props.onDeleteMessage(id)}
							onChangeMessage={(id, text) => props.onChangeMessage(id, text)}
						/>
					)}
					<input ref={input => input && input.focus()}/>
				</div>
			}
		</div>
	);
}

export default MessageList;