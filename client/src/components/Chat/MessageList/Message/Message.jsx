import React, {useState} from 'react';
import './message.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faThumbsUp} from '@fortawesome/free-solid-svg-icons'
import Moment from "react-moment";

const Message = props => {
	const [isLike, setIsLike] = useState(false);

	const onLike = () => {
		setIsLike(!isLike);
	}

	const {message} = props;
	const likeStyle = isLike ? 'message-liked-icon' : 'like-icon';
	const likeStyleButton = isLike ? 'message-liked' : 'message-like';

	return (
		<div className='message'>

			<div className='message-user-avatar'>
				<img src={message.avatar} alt="avatar"/>
			</div>

			<div className='message-text'>
				{message.text}
			</div>

			<div className='user-data-block'>
				<div className='message-user-name'>{message.user}</div>
				<div className='message-time'>
					<Moment
						style={{color: '#775'}}
						format="HH:mm"
					>
						{message.createdAt}
					</Moment>
				</div>
			</div>

			<div className='message-like-block'>
				<button
					className={likeStyleButton}
					onClick={() => onLike()}
				>
					<FontAwesomeIcon
						icon={faThumbsUp}
						className={likeStyle}
					/>
				</button>
			</div>

		</div>
	);
}

export default Message;