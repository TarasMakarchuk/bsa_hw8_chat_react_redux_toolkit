import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSpinner} from '@fortawesome/free-solid-svg-icons'
import './preloader.css';

const Preloader = () => {
	return (
		<div className='preloader'>
			<FontAwesomeIcon icon={faSpinner} className='spinner'/>
		</div>
	);
}

export default Preloader;