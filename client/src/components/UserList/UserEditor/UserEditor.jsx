import React from 'react';
import './userEditor.css';

const UserEditor = props => {

	const onSubmitHandler = event => {
		event.preventDefault();
	}

	return (
		<div className='edit-user'>
			<div className='form'>
				<h3>Edit user</h3>

				<form className="edit-form">
					<div className='edit-form-input-container'>
						<div>
						</div>
						<input className='input-username' type="text" placeholder="username"/>
					</div>

					<div className='edit-form-input-container'>
						<div>
						</div>
						<input className='input-login' type="text" placeholder="email"/>
					</div>

					<div className='edit-form-input-container'>
						<div>
						</div>
						<input className='input-role' type="text" placeholder="role - user or admin"/>
					</div>

					<div className='edit-form-input-container'>
						<div>
						</div>
						<input className='input-password' type="password" placeholder="password"/>
					</div>

					<div className='edit-buttons-block'>
						<button onClick={onSubmitHandler} type='submit' className='edit-user-save-button'>Save</button>
						<button
							onClick={props.handleCancel}
							type='reset' className='edit-user-cancel-button'>Cancel
						</button>
					</div>
				</form>

			</div>
		</div>
	)
};

export default UserEditor;