import React from 'react';
import './userInfoBlock.css';

const UserInfoBlock = props => {

	console.log('props.role');
	console.log(props);

	return (
		<div className='user-info'>
			<div className='user-info-block'>
				<div className='user-name'>
					<h3>{props.userName}</h3>
				</div>
				<div className='user-email'>
					<h3>{props.email}</h3>
				</div>
				<div className='user-role'>
					<h3>{props.userRole}</h3>
				</div>
				<div className='user-info-buttons-block'>
					<button onClick={props.handleEditUser} className='user-info-edit-button'>Edit</button>
					<button onClick={() => props.onDeleteUser(props.userId)} className='user-info-delete-button'>Delete</button>
				</div>
			</div>
		</div>
	)
};

export default UserInfoBlock;