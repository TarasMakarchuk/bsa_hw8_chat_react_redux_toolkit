import React, {useEffect} from 'react';
import './userList.css';
import UserInfoBlock from "./UserInfoBlock/UserInfoBlock";
import AddUser from "./AddUser/AddUser";
import UserEditor from "./UserEditor/UserEditor";
import SignOut from "../SignOut/SignOut";
import BackButton from "../BackButton/BackButton";
import * as userActions from "../UserList/userListActions";
import * as chatActions from "../Chat/chatActions";
import {useDispatch, useSelector} from "react-redux";
import Preloader from "../Preloader/Preloader";
import axios from "axios";

const UserList = () => {

	const API_URL_USERS = `http://localhost:3050/api/users`;
	const [isShowAddUser, setShowAddUser] = React.useState(false);
	const [isShowEditUser, setShowEditUser] = React.useState(false);

	const { users } = useSelector(state => state.rootReducer.usersReducer);
	const { preloader } = useSelector(state => state.rootReducer.chatReducer);

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(chatActions.showPreloader(true));
		fetch(API_URL_USERS)
			.then(response => response.json())
			.then(data => {

				if (data !== undefined && data !== null) {
					dispatch(userActions.addAllUsers(data));
					setTimeout(() => {
						dispatch(chatActions.showPreloader(false));
					}, 10);
				}
			})
			.catch(error => console.error(error));
	}, [dispatch]);

	const handleShowModalAddUser = () => {
		setShowAddUser(true);
	};

	const usersList = [];
	if (users) {
		usersList.push(...users);
	}

	const handleCancel = () => {
		setShowAddUser(false);
		setShowEditUser(false);
	};

	const handleEditUser = () => {
		setShowEditUser(true);
	};

	const onDeleteUser = async id => {
		dispatch(userActions.deleteUser(id));
		await axios.delete(API_URL_USERS + `/${id}`);
	};

	return (
		<div className='user-list-page-container'>

			<div className='user-list-page-sign-out-button-block'>
				<BackButton/>
				<SignOut/>
			</div>
			<div className='user-list-page'>

				<h2>Admin page</h2>

				<div className='user-list-page-buttons-block'>
					<button
						onClick={handleShowModalAddUser}
						className='add-user-button'
					>Add user
					</button>
				</div>

				<div className='users-info-blocks'>
					{isShowAddUser &&
					<AddUser
						handleCancel={handleCancel}
						setShowAddUser={setShowAddUser}
					/>}
					{isShowEditUser && <UserEditor handleCancel={handleCancel}/>}
					{ !preloader && <Preloader /> }
					{usersList.map(item =>
						<UserInfoBlock
							key={Date.now() + Math.random()}
							userName={item.userName}
							email={item.email}
							userId={item.id}
							userRole={item.role}
							handleCancel={handleCancel}
							handleEditUser={handleEditUser}
							onDeleteUser={id => onDeleteUser(id)}
						/>
					)}
				</div>

			</div>
		</div>
	);
};

export default UserList;