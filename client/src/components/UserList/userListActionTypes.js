export const ADD_ALL_USERS = 'ADD_ALL_USERS';
export const SET_USER_ROLE = 'SET_USER_ROLE';
export const ADD_USER = 'ADD_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const DELETE_USER = 'DELETE_USER'