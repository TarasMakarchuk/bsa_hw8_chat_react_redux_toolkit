import {
	ADD_ALL_USERS,
	ADD_USER,
	UPDATE_USER,
	DELETE_USER, SET_USER_ROLE
} from './userListActionTypes';

const initialState = {
	chat: {
		users: [
			{
				"userName": "admin",
				"email": "admin",
				"password": "admin",
				"role": "admin",
				"id": "f0a647ae-50b7-4a7b-832f-4241f4d2a439",
				"createdAt": "2021-07-11T13:16:46.676Z"
			},
			{
				"userName": "user",
				"email": "user",
				"password": "user",
				"role": "user",
				"id": "bd0a379e-f06c-4f63-a59d-c9da7253f9b9",
				"createdAt": "2021-07-11T13:18:05.907Z"
			}
		],
		role: ''
	}
}

export default function usersReducer(state = initialState, action) {
	switch (action.type) {

		case ADD_ALL_USERS: {
			const {data} = action.payload;
			return {
				...state.chat,
				users: [...state.chat.users, ...data]
			};
		}

		case ADD_USER: {
			const { data } = action.payload
			return {
				...state.chat,
				users: [...state.users, data]
			};
		}

		case SET_USER_ROLE: {
			const { data } = action.payload
			return {
				...state,
				role: data,
			};
		}

		case UPDATE_USER: {
			const {id, data} = action.payload;
			const updateUsers = [];
			updateUsers.push(...state.users);
			const index = updateUsers.findIndex(item => item.id === id);
			updateUsers[index].userName = data.userName;
			updateUsers[index].email = data.email;
			updateUsers[index].password = data.password;
			updateUsers[index].role = data.role;

			return {
				...state.chat,
				users: updateUsers,
			};

		}

		case DELETE_USER: {
			const {id} = action.payload;
			return {
				...state.chat,
				users: [...state.users.filter(message => message.id !== id)]
			};
		}

		default:
			return state;
	}
}