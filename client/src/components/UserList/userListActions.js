import {
	ADD_ALL_USERS,
	ADD_USER,
	UPDATE_USER,
	DELETE_USER,
	SET_USER_ROLE
} from './userListActionTypes';

export const addAllUsers = data => ({
	type: ADD_ALL_USERS,
	payload: {
		data
	}
});


export const addUser = data => ({
	type: ADD_USER,
	payload: {
		data
	}
});


export const setUserRole = data => ({
	type: SET_USER_ROLE,
	payload: {
		data
	}
});

export const updateUser = (id, data) => ({
	type: UPDATE_USER,
	payload: {
		id,
		data
	}
});

export const deleteUser = id => ({
	type: DELETE_USER,
	payload: {
		id
	}
});
