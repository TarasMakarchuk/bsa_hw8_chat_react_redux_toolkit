import React, {useRef} from 'react';
import './addUser.css';
import axios from "axios";
import * as userActions from "../userListActions";
import {useDispatch} from "react-redux";

const AddUser = props => {

	const formRef = useRef();
	const dispatch = useDispatch();

	const handleCreateUser = event => {
		event.preventDefault();
		const userData = [];
		for (const key of formRef.current.elements) {
			userData.push(key.value);
		}
		userData.length = 4;

		let newUser;
		if (userData !== null & userData[0] !== '' && userData[1] !== ''
			&& userData[2] !== '' && userData[3] !== '') {
			newUser = {
				userName: userData[0],
				email: userData[1],
				role: userData[2],
				password: userData[3],
			};
		}
		handleAddNewUser(newUser);
		props.setShowAddUser(false);
	}

	const handleAddNewUser = (newUser) => {
		axios.post('http://localhost:3050/api/users', newUser)
			.then(response => {
				const data = response.data;
				if (data !== null) {
					dispatch(userActions.addUser(data));
				}
			})
			.catch(error => console.error(error));
	}

	return (
		<div className='add-user'>
			<div className='add-user-container'>
				<div className='form'>
					<h3>Add new user</h3>

					<form ref={formRef} onSubmit={handleCreateUser} className="add-user-form">
						<div className='add-user-form-input-container'>
							<div>
							</div>
							<input className='input-username' type="username" placeholder="username"/>
						</div>

						<div className='add-user-form-input-container'>
							<div>
							</div>
							<input className='input-login' type="email" placeholder="email"/>
						</div>

						<div className='add-user-form-input-container'>
							<div>
							</div>
							<input className='input-login' type="tetx" placeholder="role - user or admin"/>
						</div>

						<div className='add-user-form-input-container'>
							<div>
							</div>
							<input className='input-password' type="password" placeholder="password"/>
						</div>

						<div className='add-user-buttons-block'>
							<button
								onClick={props.handleCancel}
								type='reset' className='add-user-cancel-button'
							>
								Cancel
							</button>
							<button type='submit' className='add-user-create-button'>Create</button>
						</div>
					</form>
				</div>

			</div>
		</div>
	)
};

export default AddUser;