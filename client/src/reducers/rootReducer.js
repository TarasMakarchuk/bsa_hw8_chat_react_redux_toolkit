import { combineReducers } from 'redux';
import chatReducer from '../components/Chat/chatReducer';
import usersReducer from "../components/UserList/userListReducer";

const rootReducer = combineReducers({
	chatReducer,
	usersReducer
});

export default rootReducer;