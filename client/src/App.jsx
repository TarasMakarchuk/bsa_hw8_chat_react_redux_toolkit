import React from 'react';
import './index.css';
import ErrorBoundary from "./error/ErrorBoundary";
import {BrowserRouter} from "react-router-dom";
import {Redirect, Route, Switch} from "react-router";
import LoginPage from "./components/LoginPage/LoginPage";
import Chat from "./components/Chat/Chat";
import UserList from "./components/UserList/UserList";
import {AppPath} from "./common/enums/enums";
import AdminPage from "./components/AdminPage/AdminPage";

function App() {
	return (
		<div className="App">
			<ErrorBoundary>
				<BrowserRouter>
					<Redirect to='/login'/>
						<Switch>
							<Route path={AppPath.LOGIN} exact component={LoginPage}/>
							<Route path={AppPath.CHAT} component={Chat}/>
							<Route path={AppPath.USER_LIST} exact component={UserList}/>
							<Route path={AppPath.ADMIN} exact component={AdminPage}/>
							<Route path={AppPath.ANY} exact component={LoginPage}/>
						</Switch>
				</BrowserRouter>
			</ErrorBoundary>
		</div>
	);
}

export default App;
